<?php

/**
 * @file
 * Contains \Drupal\persona\Form\PersonaForm.
 */

namespace Drupal\persona\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Persona edit forms.
 *
 * @ingroup persona
 */
class PersonaForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\persona\Entity\Persona */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Persona.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Persona.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.persona.canonical', ['persona' => $entity->id()]);
  }

}
