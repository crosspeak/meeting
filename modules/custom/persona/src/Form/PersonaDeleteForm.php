<?php

/**
 * @file
 * Contains \Drupal\persona\Form\PersonaDeleteForm.
 */

namespace Drupal\persona\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Persona entities.
 *
 * @ingroup persona
 */
class PersonaDeleteForm extends ContentEntityDeleteForm {

}
