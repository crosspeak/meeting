<?php

/**
 * @file
 * Contains \Drupal\persona\Entity\Persona.
 */

namespace Drupal\persona\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Persona entities.
 */
class PersonaViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['persona']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Persona'),
      'help' => $this->t('The Persona ID.'),
    );

    return $data;
  }

}
