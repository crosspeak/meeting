<?php

/**
 * @file
 * Contains \Drupal\persona\PersonaListBuilder.
 */

namespace Drupal\persona;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Persona entities.
 *
 * @ingroup persona
 */
class PersonaListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Persona ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\persona\Entity\Persona */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.persona.edit_form', array(
          'persona' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
