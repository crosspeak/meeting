<?php

/**
 * @file
 * Contains \Drupal\persona\PersonaInterface.
 */

namespace Drupal\persona;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Persona entities.
 *
 * @ingroup persona
 */
interface PersonaInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Persona name.
   *
   * @return string
   *   Name of the Persona.
   */
  public function getName();

  /**
   * Sets the Persona name.
   *
   * @param string $name
   *   The Persona name.
   *
   * @return \Drupal\persona\PersonaInterface
   *   The called Persona entity.
   */
  public function setName($name);

  /**
   * Gets the Persona creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Persona.
   */
  public function getCreatedTime();

  /**
   * Sets the Persona creation timestamp.
   *
   * @param int $timestamp
   *   The Persona creation timestamp.
   *
   * @return \Drupal\persona\PersonaInterface
   *   The called Persona entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Persona published status indicator.
   *
   * Unpublished Persona are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Persona is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Persona.
   *
   * @param bool $published
   *   TRUE to set this Persona to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\persona\PersonaInterface
   *   The called Persona entity.
   */
  public function setPublished($published);

}
