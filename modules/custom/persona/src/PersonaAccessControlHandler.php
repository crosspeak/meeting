<?php

/**
 * @file
 * Contains \Drupal\persona\PersonaAccessControlHandler.
 */

namespace Drupal\persona;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Persona entity.
 *
 * @see \Drupal\persona\Entity\Persona.
 */
class PersonaAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\persona\PersonaInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished persona entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published persona entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit persona entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete persona entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add persona entities');
  }

}
