<?php

/**
 * @file
 * Contains \Drupal\persona\Controller\Persona.
 */

namespace Drupal\persona\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class Persona.
 *
 * @package Drupal\persona\Controller
 */
class PersonaController extends ControllerBase {
  /**
   * Content.
   *
   * @return string
   *   Return Hello string.
   */
  public function content() {
    // Here the module attachs 'angulars' that is defined in 'inscripcion.libraries.yml'
    $output['#attached']['library'][] = 'persona/persona';
    // Theme function
    $output['#theme'] = 'form-persona';

    return $output;
  }

  
  public function content_list() {
    // Here the module attachs 'angulars' that is defined in 'inscripcion.libraries.yml'
    $output['#attached']['library'][] = 'persona/listaPersona';
    // Theme function
    $output['#theme'] = 'listaPersona';

    return $output;
  }
}
