<?php

/**
 * @file
 * Contains persona.page.inc..
 *
 * Page callback for Persona entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Persona templates.
 *
 * Default template: persona.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_persona(array &$variables) {
  // Fetch Persona Entity Object.
  $persona = $variables['elements']['#persona'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
