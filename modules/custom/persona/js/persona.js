/**
 * @file
 *  Related JavaScript.
 */
// Declare an app.
var registration = angular.module('registration', []);

/**
 * Declare the controller that adds the default value to scope var.
 */
registration.controller('inscripcion', ['$scope', '$http', '$location','$window', mollo]);

function mollo($scope, $http, $location, $window) {
  // Get protocol concatenated to host
  $scope.my_location = $location.protocol() + "://" + $location.host();
  console.log($scope.my_location);

  // Get token from url
  $scope.url = $scope.my_location + "/rest/session/token";
  $http.get($scope.url).success(function(respuesta){
    $scope.token = respuesta;
    console.log($scope.token);
  });

  $scope.submit = function(){
    var t0 = performance.now();
    if($scope.name == "" || $scope.cantidad == ""){
        alert("Inserte todos los datos");
    }
    else{

      for (var i = 0; i < $scope.cantidad; i++) {
        var item  = {
          'name' : { 'value': $scope.name + " # " + i },
          '_links': { 'type': { 'href': $scope.my_location + '/rest/type/persona/persona' }}
        };
        $scope.addItem = function(l){
          console.log("Entro a guardar persona");
          return $http({
            url: $scope.my_location + '/entity/persona',
            method: 'POST',
            data: l, // pass the data object as defined above
            headers: {
              "Authorization": "Basic ZHJ1cGFsOmRydXBhbA==", // encoded user/pass - this is admin/123qwe
              "Content-Type": "application/hal+json", 
              "X-CSRF-Token": $scope.token // url/rest/session/token
            },
          })
        };
        $scope.addItem(item);
      }
      var t1 = performance.now();
      $scope.name = $scope.cantidad = "";
    }
    $scope.tiempo = (t1 - t0);
    console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.");
  };

};

/**
 * We need to bootstrap the app manually to the container by id, since we have
 *  more than one app on the same page.
 */
angular.element(document).ready(function() {
                                            //ID            APP
  angular.bootstrap(document.getElementById("content-form-registration"),['registration']);
});