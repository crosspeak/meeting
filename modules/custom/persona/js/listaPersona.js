/**
 * @file
 *  Related JavaScript.
 */
// Declare an app.
var registration = angular.module('registration', []);

/**
 * Declare the controller that adds the default value to scope var.
 */
registration.controller('inscripcion', ['$scope', '$http', '$location','$window', mollo]);

function mollo($scope, $http, $location, $window) {
  // Get protocol concatenated to host
  $scope.my_location = $location.protocol() + "://" + $location.host();
  console.log($scope.my_location);

  // Get token from url
  $scope.url = $scope.my_location + "/rest/session/token";
  $http.get($scope.url).success(function(respuesta){
    $scope.token = respuesta;
    console.log($scope.token);
  });

  $scope.urlPersona = "export_personas";
  $http.get($scope.urlPersona).success(function(respuesta){
    $scope.resPersonas = respuesta;
    console.log($scope.resPersonas);
  });

};

/**
 * We need to bootstrap the app manually to the container by id, since we have
 *  more than one app on the same page.
 */
angular.element(document).ready(function() {
                                            //ID            APP
  angular.bootstrap(document.getElementById("content-form-registration"),['registration']);
});